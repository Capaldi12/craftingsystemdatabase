-- deleting of database

use farmershelper;

-- firstly we need to drop tables, that reference other tables

DROP TABLE `fields_to_fertilizers`;
DROP TABLE `fertilizers_to_crops`;
DROP TABLE `fertilizers_to_storages`;
DROP TABLE `crops_to_fields`;
DROP TABLE `crops_to_types`;

-- then - other tables

DROP TABLE `crops`;
DROP TABLE `crop_types`;
DROP TABLE `fertilizers`;
DROP TABLE `fields`;
DROP TABLE `storages`;

-- and lastly

DROP DATABASE farmershelper;



