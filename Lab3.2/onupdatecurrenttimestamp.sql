-- Создать табличку которая будет иметь дефолтную дату - текущую
-- Первичный через альтер адд констраинт
-- При изменении дата тоже дефолтится

CREATE TABLE IF NOT EXISTS `test` (
	`id` INT NOT NULL,
    `record` VARCHAR(20) NOT NULL,
    `update_time` TIMESTAMP NOT NULL 
		DEFAULT CURRENT_TIMESTAMP()
        ON UPDATE CURRENT_TIMESTAMP()
);
ALTER TABLE `test` ADD CONSTRAINT `Primary_id` PRIMARY KEY(`record`);

SHOW CREATE TABLE `test`;

INSERT INTO `test` (`id`, `record`) VALUES (1, 'Record1'), (2, 'RecordTwo');
SELECT * FROM `test`;

UPDATE `test` SET `record` = 'RecordTHREE' WHERE `record` = 'Record1';
SELECT * FROM `test`;

DROP TABLE `test`;