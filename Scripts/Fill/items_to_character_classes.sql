INSERT INTO items_to_character_classes (item_id, class_id)
VALUES ((SELECT id FROM items WHERE name='Wooden Sword'), (SELECT id FROM character_classes WHERE name='Warrior')),
((SELECT id FROM items WHERE name='Copper Sword'), (SELECT id FROM character_classes WHERE name='Warrior')),
((SELECT id FROM items WHERE name='Copper Shortsword'), (SELECT id FROM character_classes WHERE name='Warrior')),
((SELECT id FROM items WHERE name='Copper Shortsword'), (SELECT id FROM character_classes WHERE name='Rogue')),
((SELECT id FROM items WHERE name='Terra Blade'), (SELECT id FROM character_classes WHERE name='Warrior')),
((SELECT id FROM items WHERE name='Shield of Cthulhu'), (SELECT id FROM character_classes WHERE name='Warrior')),
((SELECT id FROM items WHERE name='Shield of Cthulhu'), (SELECT id FROM character_classes WHERE name='Rogue'));
SELECT * FROM items_to_character_classes;