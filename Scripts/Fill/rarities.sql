INSERT INTO rarities (color, name)
VALUES (8421504, 'Gray'),
(16777215, 'White'),
(8628991, 'Blue'),
(8453484, 'Green'),
(16630892, 'Orange'),
(16474465, 'Light red'),
(16736969, 'Pink'),
(14311931, 'Light purple'),
(6815609, 'Lime'),
(16773888, 'Yellow'),
(65535, 'Cyan'),
(16711680, 'Red'),
(8388863, 'Purple'),
(-1, 'Rainbow'),
(-2, 'Fiery-red'),
(16760576, 'Amber');
SELECT * FROM rarities;