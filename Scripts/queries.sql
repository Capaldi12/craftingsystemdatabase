-- Запросы по функциональным требованиям к БД

# 1 Поиск всех рецептов для создания заданного предмета
SELECT * FROM recipes WHERE product_id=(SELECT id FROM items WHERE name = 'Copper Bar');

# 2 Поиск всех рецептов, использующих заданный предмет
-- Original
SELECT id AS recipe_id, product_id FROM
	(
		recipes
	JOIN
		(SELECT recipe_id, item_id FROM recipes_to_ingredients) AS rtoi
	ON recipes.id = rtoi.recipe_id
	)
WHERE item_id in (SELECT id FROM items WHERE name = 'id');

-- Rewrite
SELECT recipe_id, product_id FROM
	recipes JOIN recipes_to_ingredients ON recipes.id = recipe_id
		JOIN items ON items.id = item_id
			WHERE items.name = 'id';
            
-- Just by id
SELECT recipe_id, product_id FROM
	recipes JOIN recipes_to_ingredients ON recipes.id = recipe_id
		WHERE item_id = '2';


# 3 Поиск всех предметов, использующих заданный предмет в своих рецептах
SELECT DISTINCT id, name FROM
	(
		(
		SELECT  product_id FROM
			(
				recipes
			JOIN
				(SELECT recipe_id, item_id FROM recipes_to_ingredients) AS rtoi
			ON recipes.id = rtoi.recipe_id
			)
		WHERE item_id = (SELECT id FROM items WHERE name = 'Wood')
		) AS itemids
	JOIN
		items
	ON items.id = itemids.product_id
	)
ORDER BY name;

-- Other way round
SELECT DISTINCT items.id, items.name FROM
	items AS ingredients 
        JOIN recipes_to_ingredients ON ingredients.id = recipes_to_ingredients.item_id
		JOIN recipes ON recipes.id = recipe_id
        JOIN items ON items.id = product_id
	WHERE ingredients.name = 'morbi vestibulum'
ORDER BY items.name;
	
# 4 Поиск предметов, которые можно создать на заданном рабочем месте
SELECT DISTINCT id, name FROM
	(
		items
	JOIN
		(
		SELECT product_id FROM
			(
				recipes
			JOIN
				(
				SELECT recipe_id FROM recipes_to_workbenches
				WHERE workbench_id = (SELECT id FROM workbenches WHERE name = 'Anvil')
				) AS rtow
			ON recipes.id = rtow.recipe_id
			)
		) AS recipesel
	ON items.id = recipesel.product_id
	)
ORDER BY name;

-- rewrite	-- search by id
SELECT DISTINCT items.id, items.name FROM
	items JOIN recipes ON product_id = items.id
		JOIN recipes_to_workbenches ON recipes.id = recipe_id
			WHERE workbench_id = 1;

# 5 Поиск рецептов, которые можно создать на заданном рабочем месте
SELECT id AS recipe_id, product_id FROM
	(
		recipes
	JOIN
		(
		SELECT recipe_id FROM recipes_to_workbenches
		WHERE workbench_id = (SELECT id FROM workbenches WHERE name = 'Anvil')
		) AS rtow
	ON recipes.id = rtow.recipe_id
	);
	
# 6 Поиск предметов, имеющих заданный тип урона
SELECT id, name, value FROM
	(
		items
	JOIN
		(
		SELECT item_id, value FROM items_to_damage_types
		WHERE damage_type_id = (SELECT id FROM damage_types WHERE name = 'Melee')
		) AS ioftype
	ON items.id = ioftype.item_id
	)
ORDER BY name;
	
# 7 Поиск предметов, имеющих заданный тип защиты
SELECT id, name, value FROM
	(
		items
	JOIN
		(
		SELECT item_id, value FROM items_to_defense_types
		WHERE defense_type_id = (SELECT id FROM defense_types WHERE name = 'Standard')
		) AS ioftype
	ON items.id = ioftype.item_id
	)
ORDER BY name;
	
# 8 Поиск предметов, связанных с заданным эффектом
SELECT id, name, duration FROM
	(
		items
	JOIN
		(
			SELECT item_id, duration FROM items_to_effects
			WHERE effect_id = (SELECT id FROM effects WHERE name = 'in libero ut')
		) AS itoe
	ON items.id = itoe.item_id
	)
ORDER BY name;
	
# 9 Поиск предметов, подходящих заданному классу
SELECT id, name FROM
	(
		items
	JOIN
		(
		SELECT item_id FROM items_to_character_classes
		WHERE class_id = (SELECT id FROM character_classes WHERE name = 'Rogue')
		) as itoc
	ON items.id = itoc.item_id
	)
ORDER BY name;
	
# 10 Поиск предметов с заданной редкостью
SELECT id, name FROM items WHERE rarity_id = (SELECT id FROM rarities WHERE name='Yellow') ORDER BY name;

# 11 Поиск предметов, имеющих определенное значение урона
SELECT id, name FROM
	(
		items
	JOIN
		(SELECT item_id FROM items_to_damage_types WHERE value = 3) AS itod
	ON items.id = itod.item_id
	)
ORDER BY name;

# 12 Поиск предметов, имеющих определенное значение защиты 
SELECT id, name FROM
	(
		items
	JOIN
		(SELECT item_id FROM items_to_defense_types WHERE value = 2) AS itod
	ON items.id = itod.item_id
	)
ORDER BY name;

# 13 Поиск предметов с заданной стоимостью
SELECT id, name FROM items WHERE base_price = 0;

# 14 Поиск предмета с самым большим уроном
SELECT id, name FROM items WHERE id = 
	(SELECT item_id FROM items_to_damage_types WHERE value = 
		(SELECT MAX(value) FROM items_to_damage_types));

# 15 Поиск предмета с самой большой защитой
SELECT id, name FROM items WHERE id = 
	(SELECT item_id FROM items_to_defense_types WHERE value = 
		(SELECT MAX(value) FROM items_to_defense_types));
		
# 16 Поиск предмета с самой большой стоимостью
SELECT id, name, base_price FROM items WHERE base_price = (SELECT MAX(base_price) FROM items);

# 17 Найти все предметы, которые могут выступать в роли заданного верстака
SELECT id, name FROM
	(SELECT item_id AS id FROM workbenches_to_items WHERE workbench_id = 
		(SELECT id FROM workbenches WHERE name = 'amet eros')) AS wti
NATURAL JOIN
	items
ORDER BY name;
