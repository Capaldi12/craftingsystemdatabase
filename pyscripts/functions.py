import os


baseline = 'INSERT INTO {table_name} ({columns})\nVALUES {values};\n' \
           'SELECT * FROM {table_name};'
selectline = '(SELECT {col} FROM {table_name} WHERE {col2}={value})'


def c_int(color: tuple = (0, 0, 0)) -> int:
    """Returns integer RGB representation of given (R, G, B) color"""
    red, green, blue = color
    return (red << 16) + (green << 8) + blue


def c_tuple(color: int) -> tuple:
    """Returns (R, G, B) representation of given integer RGB color"""
    return (color >> 16) & 255, (color >> 8) & 255, color & 255


def select(col, table, cond, value):
    """Creates SELECT statement to use inside INSERT"""
    dc = {'col': col, 'table_name': table, 'col2': cond, 'value': value}
    return selectline.format(**dc)


def q(s: str) -> str:
    """Quotes string"""
    return '\'' + s + '\''


def makepath(filename: str) -> str:
    """Formats given filename to path"""
    if '.sql' not in filename:
        filename += '.sql'

    if ':' not in filename:
        if not os.path.exists(os.getcwd() + '\\output'):
            os.mkdir(os.getcwd() + '\\output')

        filename = os.getcwd() + '\\output\\' + filename

    return filename


def make_script(filename: str, content: dict):
    """Creates INSERT script using given data and saves it to the file"""
    line = baseline.format(**content)
    with open(makepath(filename), mode='w+') as file:
        file.write(line)


def make_recipe(product, amount, ingredients, workbenches):
    """Returns items to add in recipe-related tables"""
    r_item = (product, amount)
    s = select('id', 'items', 'name', q(product))
    i_items = [(s, i[0], i[1]) for i in ingredients]
    w_items = [(s, w) for w in workbenches]

    return r_item, i_items, w_items


if __name__ == '__main__':

    ...
