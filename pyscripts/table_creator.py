import csv

from functions import *
import contents


def parse_table(table_data: dict) -> dict:
    """Creates dictionary to insert in INSERT command"""

    res = {'table_name': table_data['name'], 'columns': ', '.join(table_data['columns']), 'values': ''}
    col_count = len(table_data['columns'])
    types = table_data['types']
    fks = table_data['fks']

    vstring = '(' + ', '.join(['{}'] * col_count) + ')'
    vstrings = []

    for item in table_data['items']:
        item = list(item)
        for i in range(col_count):
            if types[i] == 'str':
                item[i] = q(item[i])
            elif types[i] == 'fk':
                if item[i] != 'NULL':
                    item[i] = select(fks[i][0], fks[i][1], fks[i][2], q(item[i]) if fks[i][3] == 'str' else item[i])

        vstrings += [vstring.format(*item)]

    res['values'] = ',\n'.join(vstrings)

    return res


def parse_table_from_csv(base_table: dict, filename: str) -> dict:
    """Creates dictionary to insert in INSERT command using data in .csv file"""
    new_table = dict(base_table)
    with open(filename, 'r') as file:
        reader = csv.reader(file)

        for line in reader:
            for i in range(len(line)):
                if '.png' in line[i] or '.gif' in line[i]:
                    line[i] = contents.p + line[i]

            new_table['items'].append(tuple(line))

    return parse_table(new_table)


def csv_to_sql(base_table: dict, filename: str) -> None:
    make_script(filename + '.sql', parse_table_from_csv(base_table, filename))


if __name__ == '__main__':
    # for table_data in contents.content_list:
    #     make_script(table_data['name'], parse_table(table_data))

    csv_to_sql(contents.b_items_to_effects, 'items_to_effects.csv')
    ...
