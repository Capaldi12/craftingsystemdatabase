import sys
import traceback
from PyQt5.QtWidgets import QApplication, QMessageBox
from MainWindow import MainWindow


def exc_hook(exc_type, exc_value, exc_tb):
    traceback.print_exception(exc_type, exc_value, exc_tb)
    error_message = 'During execution of application, an error has occurred:\n\n' + \
                    ''.join(traceback.format_exception(exc_type, exc_value, exc_tb)) + \
                    '\nDo you want to continue using application (may cause data damage)?'
    if QMessageBox.question(w, 'Error', error_message,
                            QMessageBox.Yes | QMessageBox.No) == QMessageBox.No:
        QApplication.quit()


if __name__ == '__main__':
    sys.excepthook = exc_hook

    a = QApplication([])
    w = MainWindow()

    w.show()
    a.exec()
